# Wallapop test

## Architecture
In this problem I applied the [Entity-Component-System architecture](https://en.wikipedia.org/wiki/Entity_component_system).
I found this architecture suitable for the problem because:

  * The problem can be oriented as a Game problem.
  * It is easy to extend, add new rovers, new type of obstacles, etc.
  * It is readable: in my opinion, its easy to follow the flow and understand the behaviour.
  * It is fast: in this case the bottleneck is the CollisionSubsystem and it takes only O(N) time where N is the
  number of positions (rover + obstacles). 
  
## Run
To run the app you can use `gradle run` or with docker:
```
docker run -ti --rm -v "$PWD":/srv -w /srv gradle gradle run
``` 

## Comments
I want to add the following comments regarding the code:

  * Java is not my main stack so there may be a lot of things that could be improved.
  * Its the first time that I use the Entity-Component-System architecture.
  * I don't implemented the first input validation.
  * I don't implemented all the unit tests because of lack of time.
  * It should be easy to use a Dependency Injection framework as almost all classes have their own constructor with the
  required parameters.