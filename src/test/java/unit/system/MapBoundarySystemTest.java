package unit.system;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wallapop.component.Input;
import wallapop.component.Position;
import wallapop.component.Size;
import wallapop.shared.Direction;
import wallapop.shared.entity_manager.EntityManager;
import wallapop.system.MapBoundarySystem;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MapBoundarySystemTest {

    private MapBoundarySystem sut;

    @BeforeEach
    void setUp() {
        sut = new MapBoundarySystem(new Size(0, 0, 5, 5));
    }

    @Test
    void testShouldMoveToEndWhenLongitudeLessThanMapSize() {
        UUID roverUuid = UUID.randomUUID();
        Position roverPosition = new Position(-1, 0);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Input> inputs = new HashMap<>();
        inputs.put(roverUuid, new Input(0, Direction.SOUTH));
        when(entityManager.getEntityComponentMapByClass(Input.class)).thenReturn(inputs);
        when(entityManager.getComponent(roverUuid, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(roverPosition.x(), 5);
    }

    @Test
    void testShouldMoveToEndWhenLatitudeLessThanMapSize() {
        UUID roverUuid = UUID.randomUUID();
        Position roverPosition = new Position(0, -1);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Input> inputs = new HashMap<>();
        inputs.put(roverUuid, new Input(0, Direction.SOUTH));
        when(entityManager.getEntityComponentMapByClass(Input.class)).thenReturn(inputs);
        when(entityManager.getComponent(roverUuid, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(roverPosition.y(), 5);
    }

    @Test
    void testShouldMoveToBegginingWhenLongitudeGreaterThanMapSize() {
        UUID roverUuid = UUID.randomUUID();
        Position roverPosition = new Position(6, 0);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Input> inputs = new HashMap<>();
        inputs.put(roverUuid, new Input(0, Direction.SOUTH));
        when(entityManager.getEntityComponentMapByClass(Input.class)).thenReturn(inputs);
        when(entityManager.getComponent(roverUuid, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(roverPosition.x(), 0);
    }

    @Test
    void testShouldMoveToBegginingWhenLatitudeGreaterThanMapSize() {
        UUID roverUuid = UUID.randomUUID();
        Position roverPosition = new Position(0, 6);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Input> inputs = new HashMap<>();
        inputs.put(roverUuid, new Input(0, Direction.SOUTH));
        when(entityManager.getEntityComponentMapByClass(Input.class)).thenReturn(inputs);
        when(entityManager.getComponent(roverUuid, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(roverPosition.y(), 0);
    }

    @Test
    void testShouldDoNothingWhenPositionInsideMap() {
        UUID roverUuid = UUID.randomUUID();
        Position roverPosition = new Position(5, 5);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Input> inputs = new HashMap<>();
        inputs.put(roverUuid, new Input(0, Direction.SOUTH));
        when(entityManager.getEntityComponentMapByClass(Input.class)).thenReturn(inputs);
        when(entityManager.getComponent(roverUuid, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(roverPosition.x(), 5);
        assertEquals(roverPosition.y(), 5);
    }
}