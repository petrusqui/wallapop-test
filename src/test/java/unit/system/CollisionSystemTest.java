package unit.system;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wallapop.component.Position;
import wallapop.component.Size;
import wallapop.shared.CollisionException;
import wallapop.shared.SystemOutput;
import wallapop.shared.entity_manager.EntityManager;
import wallapop.system.CollisionSystem;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CollisionSystemTest {

    private CollisionSystem sut;
    private UUID rover;
    private Size mapSize;
    private SystemOutput output;

    @BeforeEach
    void setUp() {
        rover = UUID.randomUUID();
        mapSize = new Size(0, 0, 5, 5);
        output = mock(SystemOutput.class);
        sut = new CollisionSystem(rover, mapSize, output);
    }

    @Test
    void testShouldMoveRoverUpWhenThereIsAnObstacle()
    {
        Position roverPosition = new Position(0, 1);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Position> positions = new HashMap<>();
        positions.put(UUID.randomUUID(), new Position(0, 1));
        positions.put(UUID.randomUUID(), new Position(0, 2));
        when(entityManager.getEntityComponentMapByClass(Position.class)).thenReturn(positions);
        when(entityManager.getComponent(rover, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(0, roverPosition.x());
        assertEquals(3, roverPosition.y());
    }

    @Test
    void testShouldMoveRoverToStartIfObstacleInTheLimitOfTheMap()
    {
        Position roverPosition = new Position(0, 5);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Position> positions = new HashMap<>();
        positions.put(UUID.randomUUID(), new Position(0, 5));
        when(entityManager.getEntityComponentMapByClass(Position.class)).thenReturn(positions);
        when(entityManager.getComponent(rover, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(0, roverPosition.x());
        assertEquals(0, roverPosition.y());
    }

    @Test
    void testShouldDoNothingWhenComponentsInDifferentPositions()
    {
        Position roverPosition = new Position(0, 1);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Position> positions = new HashMap<>();
        positions.put(UUID.randomUUID(), new Position(1, 1));
        when(entityManager.getEntityComponentMapByClass(Position.class)).thenReturn(positions);
        when(entityManager.getComponent(rover, Position.class)).thenReturn(roverPosition);

        sut.processOneGameTick(entityManager, 'n');

        assertEquals(0, roverPosition.x());
        assertEquals(1, roverPosition.y());
    }

    @Test
    void testShouldThrowExceptionWhenFullOfObstacles()
    {
        Position roverPosition = new Position(0, 1);
        EntityManager entityManager = mock(EntityManager.class);
        HashMap<UUID, Position> positions = new HashMap<>();
        positions.put(UUID.randomUUID(), new Position(0, 0));
        positions.put(UUID.randomUUID(), new Position(0, 1));
        positions.put(UUID.randomUUID(), new Position(0, 2));
        positions.put(UUID.randomUUID(), new Position(0, 3));
        positions.put(UUID.randomUUID(), new Position(0, 4));
        positions.put(UUID.randomUUID(), new Position(0, 5));
        when(entityManager.getEntityComponentMapByClass(Position.class)).thenReturn(positions);
        when(entityManager.getComponent(rover, Position.class)).thenReturn(roverPosition);

        assertThrows(CollisionException.class, () -> sut.processOneGameTick(entityManager, 'n'));
    }
}