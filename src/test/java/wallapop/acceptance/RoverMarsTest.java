package wallapop.acceptance;

import org.mockito.InOrder;
import wallapop.game.RoverMars;
import wallapop.shared.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertThrows;


class RoverMarsTest {
    private RoverMars sut;
    private SystemInput input;
    private SystemOutput output;

    @BeforeEach
    void setUp() {
        input = mock(SystemInput.class);
        output = mock(SystemOutput.class);
        sut = new RoverMars(input, output);
        sut.addMap(-5, -5, 5, 5);
        sut.addObstacle(1, 1);
        sut.addRover(0, 0, Direction.NORTH);
    }

    @Test
    void testPrintCurrentMarsPosition() {
        when(input.hasNext()).thenReturn(true, false);
        when(input.read()).thenReturn('p');

        sut.start();
        verify(output).print("Rover at (0,0)");
    }

    @Test
    void testMoveForward() {
        when(input.hasNext()).thenReturn(true, false);
        when(input.read()).thenReturn('n');

        sut.start();
        verify(output).print("Rover at (0,1)");
    }

    @Test
    void testMoveBackward() {
        when(input.hasNext()).thenReturn(true, false);
        when(input.read()).thenReturn('b');

        sut.start();
        verify(output).print("Rover at (0,-1)");
    }

    @Test
    void testMoveRightToEast() {
        when(input.hasNext()).thenReturn(true, true, false);
        when(input.read()).thenReturn('r', 'n');

        sut.start();
        verify(output).print("Rover at (1,0)");
    }

    @Test
    void testMoveLeftToWest() {
        when(input.hasNext()).thenReturn(true, true, false);
        when(input.read()).thenReturn('l', 'n');

        sut.start();
        verify(output).print("Rover at (-1,0)");
    }

    @Test
    void testMoveToBeginningWhenOutsideOfMap() {
        when(input.hasNext()).thenReturn(true, true, true, true, true, true, false);
        when(input.read()).thenReturn('n', 'n', 'n', 'n', 'n', 'n');
        InOrder inOrder = inOrder(output);
        sut.start();

        inOrder.verify(output).print("Rover at (0,1)");
        inOrder.verify(output).print("Rover at (0,2)");
        inOrder.verify(output).print("Rover at (0,3)");
        inOrder.verify(output).print("Rover at (0,4)");
        inOrder.verify(output).print("Rover at (0,5)");
        inOrder.verify(output).print("Rover at (0,-5)");
    }

    @Test
    void testMoveRoverUpWhenRoverCollides() {
        when(input.hasNext()).thenReturn(true, true, true, false);
        when(input.read()).thenReturn('n', 'r', 'n');

        sut.start();

        verify(output).print("There is an obstacle");
        verify(output).print("Rover at (1,2)");
    }
}
