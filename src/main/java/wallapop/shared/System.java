package wallapop.shared;

import wallapop.shared.entity_manager.EntityManager;

public interface System {
    void processOneGameTick(EntityManager entityManager, char c);
}
