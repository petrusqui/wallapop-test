package wallapop.shared;

import java.util.ArrayList;
import java.util.Scanner;
import java.lang.System;

public class SystemInput {
    private Scanner sc;
    private ArrayList<Character> input = new ArrayList<>();

    public SystemInput() {
        sc = new Scanner(System.in);
    }


    public Character read() {
        if (input.size() == 0) {
            input = toCharArray(sc.next().trim());
        }

        char c = input.get(0);
        input.remove(0);
        return c;
    }

    public int readInt() {
        return sc.nextInt();
    }

    public boolean hasNext() {
        return input.size() > 0 || sc.hasNext();
    }

    private ArrayList<Character> toCharArray(String s) {
        ArrayList<Character> chars = new ArrayList<>();
        for (char ch: s.toCharArray()) {
            chars.add(ch);
        }

        return chars;
    }
}
