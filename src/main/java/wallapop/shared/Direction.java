package wallapop.shared;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST;

    public static Direction fromChar(char c) {
        switch (c) {
            case 'n':
                return NORTH;
            case 's':
                return SOUTH;
            case 'e':
                return EAST;
            case 'w':
                return WEST;
            default:
                throw new RuntimeException("Invalid direction");
        }
    }

    public Direction right() {
        switch (this) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            default:
                throw new RuntimeException("Direction invalid");
        }
    }

    public Direction left() {
        switch (this) {
            case NORTH:
                return WEST;
            case WEST:
                return SOUTH;
            case SOUTH:
                return EAST;
            case EAST:
                return NORTH;
            default:
                throw new RuntimeException("Direction invalid");
        }
    }
}
