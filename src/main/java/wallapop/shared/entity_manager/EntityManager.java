package wallapop.shared.entity_manager;

import wallapop.shared.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class EntityManager {
    private final HashSet<UUID> allEntities = new HashSet<>();
    private final HashMap<Class<? extends Component>, HashMap<UUID, Component>> componentMapByClass = new HashMap<>();
    private final HashMap<UUID, HashMap<Class<? extends Component>, Component>> entityComponentMap = new HashMap<>();

    public UUID createEntity() {
        UUID entity = UUID.randomUUID();
        allEntities.add(entity);
        return entity;
    }

    public boolean entityExists(UUID entity) {
        return allEntities.contains(entity);
    }

    public <T extends Component> void addComponent(UUID entity, T component) throws EntityNotFoundException {
        guardEntityExists(entity);
        if (!entityComponentMap.containsKey(entity)) {
            entityComponentMap.put(entity, new HashMap<>());
        }

        HashMap<Class<? extends Component>, Component> componentTypeMap = entityComponentMap.get(entity);
        if (componentTypeMap.containsKey(component.getClass())) {
            throw new EntityComponentAlreadyExistsException(entity, component.getClass().getCanonicalName());
        }

        HashMap<UUID, Component> entityComponentMap = componentMapByClass.get(component.getClass());
        if (entityComponentMap == null) {
            entityComponentMap = new HashMap<>();
            componentMapByClass.put(component.getClass(), entityComponentMap);
        }

        entityComponentMap.put(entity, component);
        componentTypeMap.put(component.getClass(), component);
    }

    @SuppressWarnings("unchecked")
    public <T extends Component> HashMap<UUID, T> getEntityComponentMapByClass(Class<T> componentClass) {
        if (!componentMapByClass.containsKey(componentClass)) {
            return new HashMap<>();
        }
        return (HashMap<UUID, T>) componentMapByClass.get(componentClass);
    }

    public <T extends Component> T getComponent(UUID entity, Class<T> componentClass) throws EntityNotFoundException {
        guardEntityExists(entity);
        if (!entityComponentMap.containsKey(entity)) {
            return null;
        }

        return componentClass.cast(entityComponentMap.get(entity).get(componentClass));
    }


    private void guardEntityExists(UUID entity) throws EntityNotFoundException {
        if (!entityExists(entity)) {
            throw new EntityNotFoundException("Entity " + entity + " does not exist!");
        }
    }

}