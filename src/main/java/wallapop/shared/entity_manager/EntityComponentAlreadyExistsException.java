package wallapop.shared.entity_manager;

import java.util.UUID;

public class EntityComponentAlreadyExistsException extends RuntimeException {
    public EntityComponentAlreadyExistsException(UUID entity, String component) {
        super("Entity " + entity + " already has component " + component);
    }
}
