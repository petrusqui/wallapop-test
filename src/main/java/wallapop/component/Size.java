package wallapop.component;

import wallapop.shared.Component;

public class Size implements Component {
    private int startX, startY, endX, endY;

    public Size(int startX, int startY, int endX, int endY) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }

    public int startX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int startY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int endX() {
        return endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int endY() {
        return endY;
    }

    public void setEndY(int endY) {
        this.endY = startY;
    }
}
