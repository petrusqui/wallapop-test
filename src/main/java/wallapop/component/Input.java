package wallapop.component;

import wallapop.shared.Component;
import wallapop.shared.Direction;

public class Input implements Component {
    private int force;
    private Direction direction;


    public Input(int force, Direction direction) {
        this.force = force;
        this.direction = direction;
    }

    public int force() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public Direction direction() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
