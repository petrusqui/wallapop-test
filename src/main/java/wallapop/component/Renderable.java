package wallapop.component;

import wallapop.shared.Component;
import wallapop.shared.SystemOutput;

public class Renderable implements Component {
    private SystemOutput systemOutput;

    public Renderable(SystemOutput systemOutput) {
        this.systemOutput = systemOutput;
    }

    public void draw(Position position) {
        this.systemOutput.print("Rover at " + position.toString());
    }
}
