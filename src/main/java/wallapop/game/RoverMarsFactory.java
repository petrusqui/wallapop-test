package wallapop.game;

import wallapop.shared.Direction;
import wallapop.shared.SystemInput;
import wallapop.shared.SystemOutput;


public class RoverMarsFactory {
    private static SystemInput input = new SystemInput();
    private static SystemOutput output = new SystemOutput();

    public static RoverMars create() {
        RoverMars roverMars = new RoverMars();
        addMap(roverMars);
        addObstacles(roverMars);
        addRover(roverMars);
        return roverMars;
    }

    private static void addRover(RoverMars roverMars) {
        output.print("Insert horizontal initial rover position:");
        int x = input.readInt();
        output.print("Insert vertical initial rover position:");
        int y = input.readInt();
        output.print("Insert initial rover direction (n|s|e|w):");
        Direction direction = Direction.fromChar(input.read());
        roverMars.addRover(x, y, direction);
    }

    private static void addMap(RoverMars roverMars) {
        output.print("Insert horizontal map size: ");
        int sizeX = input.readInt();
        output.print("Insert vertical map size: ");
        int sizeY = input.readInt();
        roverMars.addMap(1, 1, sizeX, sizeY);
    }

    private static void addObstacles(RoverMars roverMars) {
        output.print("Insert number of obstacles: ");
        int numberOfObstacles = input.readInt();
        for (int i = 0; i < numberOfObstacles; ++i) {
            output.print("Obstacle: " + (i + 1));
            output.print("Insert coordinate x:");
            int x = input.readInt();
            output.print("Insert coordinate y:");
            int y = input.readInt();
            roverMars.addObstacle(x, y);
        }
    }
}
