package wallapop.game;

import wallapop.component.Input;
import wallapop.component.Position;
import wallapop.component.Renderable;
import wallapop.component.Size;
import wallapop.shared.*;
import wallapop.shared.entity_manager.EntityManager;
import wallapop.system.*;

import java.util.UUID;

public class RoverMars {
    private EntityManager entityManager = new EntityManager();
    private InputSystem inputSystem = new InputSystem();
    private PhysicsSystem physicsSystem = new PhysicsSystem();
    private MapBoundarySystem mapBoundarySystem;
    private CollisionSystem collisionSystem;
    private RenderSystem renderSystem = new RenderSystem();
    private SystemInput input = new SystemInput();
    private SystemOutput output = new SystemOutput();
    private Size mapSize;
    private UUID rover;

    public RoverMars() {}

    public RoverMars(SystemInput input, SystemOutput output) {
        this.input = input;
        this.output = output;
    }

    public void start() {
        collisionSystem = new CollisionSystem(rover, mapSize, output);

        while (input.hasNext()) {
            char c = input.read();
            inputSystem.processOneGameTick(entityManager, c);
            physicsSystem.processOneGameTick(entityManager, c);
            mapBoundarySystem.processOneGameTick(entityManager, c);
            collisionSystem.processOneGameTick(entityManager, c);
            renderSystem.processOneGameTick(entityManager, c);
        }
    }

    public void addRover(int x, int y, Direction direction) {
        rover = entityManager.createEntity();
        Input roverInput = new Input(0, direction);
        entityManager.addComponent(rover, roverInput);
        entityManager.addComponent(rover, new Renderable(output));
        entityManager.addComponent(rover, new Position(x, y));
        inputSystem.addControls(roverInput);
    }

    public void addMap(int startX, int startY, int endX, int endY) {
        UUID map = entityManager.createEntity();
        mapSize = new Size(startX, startY, endX, endY);
        entityManager.addComponent(map, mapSize);
        mapBoundarySystem = new MapBoundarySystem(mapSize);
    }

    public void addObstacle(int x, int y) {
        UUID obstacle = entityManager.createEntity();
        entityManager.addComponent(obstacle, new Position(x, y));
    }
}
