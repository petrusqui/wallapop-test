package wallapop.system;

import wallapop.component.Position;
import wallapop.component.Size;
import wallapop.shared.CollisionException;
import wallapop.shared.System;
import wallapop.shared.SystemOutput;
import wallapop.shared.entity_manager.EntityManager;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class CollisionSystem implements System {
    private UUID rover;
    private SystemOutput output;
    private Size mapSize;

    public CollisionSystem(UUID rover, Size mapSize, SystemOutput output) {
        this.rover = rover;
        this.output = output;
        this.mapSize = mapSize;
    }

    public void processOneGameTick(EntityManager entityManager, char c) {
        Set<Map.Entry<UUID, Position>> positions = entityManager.getEntityComponentMapByClass(Position.class).entrySet();
        Position roverPosition = entityManager.getComponent(rover, Position.class);
        Set<String> unique_positions = new HashSet<>();

        for (Map.Entry<UUID, Position> position : positions) {
            String key = computePositionKey(position.getValue());
            if (!position.getKey().equals(rover)) {
                unique_positions.add(key);
            }
        }

        if (unique_positions.contains(computePositionKey(roverPosition))) {
            output.print("There is an obstacle");
            Position newRoverPosition = findNewPosition(roverPosition, unique_positions);
            roverPosition.setX(newRoverPosition.x());
            roverPosition.setY(newRoverPosition.y());
        }
    }

    private Position findNewPosition(Position roverPosition, Set<String> positions) {
        Position newPosition = new Position(roverPosition);
        newPosition.setY(newPosition.y() + 1);
        while (newPosition.y() != roverPosition.y()) {
            if (newPosition.y() > mapSize.endY()) {
                newPosition.setY(mapSize.startY());
            }

            String key = computePositionKey(newPosition);
            if (!positions.contains(key)) {
                return newPosition;
            }

            newPosition.setY(newPosition.y() + 1);
        }

        throw new CollisionException();
    }

    private String computePositionKey(Position position) {
        return position.x() + "," + position.y();
    }
}
