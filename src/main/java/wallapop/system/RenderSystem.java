package wallapop.system;

import wallapop.component.Position;
import wallapop.component.Renderable;
import wallapop.shared.entity_manager.EntityManager;
import wallapop.shared.System;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class RenderSystem implements System {
    public void processOneGameTick(EntityManager entityManager, char c) {
        Set<Map.Entry<UUID, Renderable>> renderableEntities = entityManager.getEntityComponentMapByClass(Renderable.class).entrySet();

        for (Map.Entry<UUID, Renderable> renderableEntity : renderableEntities) {
            Position position = entityManager.getComponent(renderableEntity.getKey(), Position.class);
            renderableEntity.getValue().draw(position);
        }
    }
}
