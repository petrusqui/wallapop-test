package wallapop.system;

import wallapop.component.Input;
import wallapop.shared.System;
import wallapop.shared.entity_manager.EntityManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class InputSystem implements System {
    private HashMap<Character, Runnable> keyMap = new HashMap<>();

    public void addControls(Input input) {
        keyMap.put('n', () -> input.setForce(1));
        keyMap.put('b', () -> input.setForce(-1));
        keyMap.put('l', () -> input.setDirection(input.direction().left()));
        keyMap.put('r', () -> input.setDirection(input.direction().right()));
    }

    public void processOneGameTick(EntityManager entityManager, char c) {
        HashMap<UUID, Input> entityInputMap = entityManager.getEntityComponentMapByClass(Input.class);
        for (Input input : new HashSet<>(entityInputMap.values())) {
            input.setForce(0);
        }
        Runnable execution = keyMap.get(c);
        if (execution != null) {
            execution.run();
        }
    }
}