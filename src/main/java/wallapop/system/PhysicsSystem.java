package wallapop.system;

import wallapop.component.Input;
import wallapop.component.Position;
import wallapop.shared.System;
import wallapop.shared.entity_manager.EntityManager;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class PhysicsSystem implements System {
    public void processOneGameTick(EntityManager entityManager, char c) {
        Set<Map.Entry<UUID, Input>> entities = entityManager.getEntityComponentMapByClass(Input.class).entrySet();
        for (Map.Entry<UUID, Input> entity: entities) {
            Input input = entity.getValue();
            if (input.force() != 0) {
                Position p = entityManager.getComponent(entity.getKey(), Position.class);
                switch (input.direction()) {
                    case NORTH:
                        p.setY(p.y() + input.force());
                        break;
                    case SOUTH:
                        p.setY(p.y() - input.force());
                        break;
                    case EAST:
                        p.setX(p.x() + input.force());
                        break;
                    case WEST:
                        p.setX(p.x() - input.force());
                        break;
                }
            }
        }
    }
}
