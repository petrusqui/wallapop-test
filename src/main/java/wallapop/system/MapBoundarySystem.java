package wallapop.system;

import wallapop.component.Input;
import wallapop.component.Position;
import wallapop.component.Size;
import wallapop.shared.System;
import wallapop.shared.entity_manager.EntityManager;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MapBoundarySystem implements System {
    private Size mapSize;

    public MapBoundarySystem(Size mapSize) {
        this.mapSize = mapSize;
    }

    public void processOneGameTick(EntityManager entityManager, char c) {
        Set<Map.Entry<UUID, Input>> movingEntities = entityManager.getEntityComponentMapByClass(Input.class).entrySet();

        for (Map.Entry<UUID, Input> movingEntity : movingEntities) {
            Position position = entityManager.getComponent(movingEntity.getKey(), Position.class);
            if (position.x() > mapSize.endX()) {
                position.setX(mapSize.startX());
            } else if (position.y() > mapSize.endY()) {
                position.setY(mapSize.startY());
            } else if (position.x() < mapSize.startX()) {
                position.setX(mapSize.endX());
            } else if (position.y() < mapSize.startY()) {
                position.setY(mapSize.endY());
            }
        }
    }

    private boolean isInsideMap(Position position, Size map) {
        return map.startX() <= position.x() &&
                position.x() <= map.endX() &&
                map.startY() <= position.y() &&
                position.y() <= map.endY();
    }
}
