package wallapop;

import wallapop.game.RoverMars;
import wallapop.game.RoverMarsFactory;

public class App {
    public static void main(String[] args) {
        RoverMars roverMars = RoverMarsFactory.create();
        roverMars.start();
    }
}
